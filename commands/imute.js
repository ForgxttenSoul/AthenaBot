module.exports = {
    name: "iMute",
    desc: "Mute someone by deleting all new messages!",
    usage: "[(un)mute/muted]",
    hidden: false,
    commands: ["imute", "mute", "unmute", "muted"],
    usages: [" "],
    prerun: (function() {
        athena.on("message", (msg) => {
            let guild = msg.guild;
            let member = guild.members.get(msg.author.id);
            let mrole = guild.roles.find("name", "<~Muted~>");
            if (!mrole) return;
            if (member.roles.has(mrole.id)) {
                setTimeout(() => {
                    msg.delete().catch($ => $);
                }, 500);
            }
        });
    }),
    run: (async function(data) {
        if (data.guild.authCheck(data.author.id) < 2) return;
        let thoseMuted = data.guild.members.map(user => user.roles.has(data.guild.roles.find("name", "<~Muted~>").id) ? user : false).filter($ => $);
        let arg1 = data.args[0];
        let mentions = data.mentions.users.array();
        let mrole = data.guild.roles.find("name", "<~Muted~>");
        if (!mrole) {
            data.guild.createRole({
                name: "<~Muted~>"
            });
            mrole = data.guild.roles.find("name", "<~Muted~>");
            if (!mrole) return (data.reply("Cannot create mute role!"));
        }
        thoseMuted = mrole.members.array();
        let fail = 0;
        if (["add", "+"].includes(arg1) || data.cmd === "mute") {
            mentions.forEach(user => {
                data.guild.members.get(user.id).addRole(mrole).catch($ => fail++);
            });
        }
        if (["del", "-"].includes(arg1) || data.cmd == "unmute") {
            mentions.forEach(user => {
                data.guild.members.get(user.id).removeRole(mrole).catch($ => fail++);
            });
        }
        if (["list", "ls", "."].includes(arg1) || data.cmd === "muted") {
            let users = [];
            thoseMuted.forEach(user => users.push(user.user.username));
            data.channel.send(`**Muted users**: \`\`\`\n${users.join("\n")}\n\`\`\``);
        }
        if (fail) data.reply(`Could not (un)mute ${fail} users!`);
    }),
    onEnable: (function() {
        //
    }),
    onDisable: (function() {
        //
    })
};
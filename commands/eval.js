module.exports = {
    name: "Eval",
    desc: "Invite me bitch!",
    usage: "[e/eval/:]",
    hidden: true,
    commands: ["e", "eval", ":"],
    usages: ["eval <code>"],
    prerun: () => {
        //
    },
    run: (async (data) => {
        if (!config.owners.includes(data.author.id)) {
            return (data.reply("Unauthorized access!").then((msg) => {
                setTimeout($ => msg.delete(), 5000);
            }).catch(console.log));
        }
        (async () => {
            return (eval(`${data.args.join(" ")};`));
        })()
        .then(output => {
            data.send(newEmbed({
                title: "**Eval Success**",
                color: 0x00ff00,
                fields: [
                    { title: "Result:", value: output !== undefined ? output : true }
                ]
            })).catch(console.log);
        })
        .catch((e) => {
            data.send(newEmbed({
                title: "**Eval Failed**",
                color: 0xff0000,
                fields: [
                    { title: "Result:", value: e.message }
                ]
            })).catch(console.log);
        });
    }),
    onEnable: function () {
        //
    },
    onDisable: function () {
        athena.commands.enable("eval");
    }
};
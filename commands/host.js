module.exports = {
    name: "Hoster",
    desc: "Get a link to where bot i shosted!",
    usage: "[host]",
    hidden: false,
    commands: ["host"],
    usages: ["host "],
    prerun: function () {
        //
    },
    run: async function (data) {
        data.send("I am hosted at https://portal.hyperexpert.com/aff.php?aff=40");
    },
    onEnable: function () {
        //
    },
    onDisable: function () {
        //
    }
};
module.exports = {
    name: "BotAuth",
    desc: "Server Controls for admins and mods of the bot for the server.",
    usage: "[cadmin/cmod]",
    hidden: false,
    commands: ["cadmin", "cmod"],
    usages: ["cadmin [add/del/ls] <mentions>", "cmod [add/del/ls] <mentions>"],
    prerun: function() {
        //
    },
    run: (async (data) => {
        if (data.cmd === "ee") { return;
            console.log(1);
            if (athena.auth.check(data.author.id, data.guild) !== 5) {
                return;
            }
            console.log(2);
            let mm = `**Final message**\n🔈 **Announcement**:\n\`\`\`\nThe old instance of this bot will be discontinued, all settings from the old instance will be transfered over  including any and all tags, etc. for help please use the command 'a+help'. It is recommended going to your nearest bot command channel if you wish to proceed further.\n\`\`\``;
            let ii = 0;
            generalChannels(athena).forEach(id => {
                console.log({id});
                setTimeout($ => {
                    try {
                        athena.channels.get(id).send(mm);
                        console.log(`Sent to ${id} : ${athena.channels.get(id).guild.name}`);
                    } catch(e) {
                        console.log(`${id} did not send : ${athena.channels.get(id).guild.name}`);
                    }
                }, ((ii++) * 1000) + 1);
            });
            return;
        }
        let {
            cmd,
            args,
            mentions
        } = data;
        if (cmd === "cadmin") {
            if (![3, 4, 5].includes(data.guild.authCheck(data.author.id))) {
                return;
            }
            if (args[0]) {
                if (["add", "+"].includes(args[0])) {
                    if (data.mentions.users.array()[0]) {
                        athena.auth.addAdmin(data.mentions.users.array()[0].id, data.guild);
                        if (athena.auth.listPerms(data.mentions.users.array()[0].id, data.guild).includes(3)) {
                            data.send("✅ User added to Server Control Admins!");
                        } else {
                            data.send("❌ Error adding user to Server Control Admins!");
                        }
                    } else {
                        data.send("❌ Error occured!");
                    }
                }
                if (["del", "-"].includes(args[0])) {
                    if (data.mentions.users.array()[0]) {
                        athena.auth.delAdmin(data.mentions.users.array()[0].id, data.guild);
                        if (!athena.auth.listPerms(data.mentions.users.array()[0].id, data.guild).includes(3)) {
                            data.send("✅ User removed from Server Control Admins!");
                        } else {
                            data.send("❌ Error removing user from Server Control Admins!");
                        }
                    } else {
                        data.send("❌ Error occured!");
                    }
                }
                if (["list", "ls", "~"].includes(args[0])) {
                    let admins = athena.auth.guilds[data.guild.id].admins;
                    admins.forEach((id, index) => {
                        admins[index] = `${athena.users.get(id).username}#${athena.users.get(id).discriminator}`;
                    });
                    data.send(`**Server Control Admins**:\`\`\`\n${admins.join(", ")}\n\`\`\``);
                }
            }
        }
        if (cmd === "cmod") {
            if (![3, 4, 5].includes(data.guild.authCheck(data.author.id))) {
                return;
            }
            if (args[0]) {
                if (["add", "+"].includes(args[0])) {
                    if (data.mentions.users.array()[0]) {
                        athena.auth.addMod(data.mentions.users.array()[0].id, data.guild);
                        if (athena.auth.listPerms(data.mentions.users.array()[0].id, data.guild).includes(2)) {
                            data.send("✅ User added to Server Control Mods!");
                        } else {
                            data.send("❌ Error adding user to Server Control Mods!");
                        }
                    } else {
                        data.send("❌ Error occured!");
                    }
                }
                if (["del", "-"].includes(args[0])) {
                    if (data.mentions.users.array()[0]) {
                        athena.auth.delMod(data.mentions.users.array()[0].id, data.guild);
                        if (!athena.auth.listPerms(data.mentions.users.array()[0].id, data.guild).includes(2)) {
                            data.send("✅ User removed from Server Control Mods!");
                        } else {
                            data.send("❌ Error removing user from Server Control Mods!");
                        }
                    } else {
                        data.send("❌ Error occured!");
                    }
                }
                if (["list", "ls", "~"].includes(args[0])) {
                    let mods = athena.auth.guilds[data.guild.id].mods;
                    mods.forEach((id, index) => {
                        mods[index] = `${athena.users.get(id).username}#${athena.users.get(id).discriminator}`;
                    });
                    data.send(`**Server Control Admins**:\`\`\`\n${mods.join(", ")}\n\`\`\``);
                }
            }
        }
    }),
    onEnable: function() {
        //
    },
    onDisable: function() {
        //
    }
};
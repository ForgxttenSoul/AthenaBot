module.exports = {
    name: "Tags",
    desc: "Keyword triggered commands.",
    usage: "[tag]",
    hidden: false,
    commands: ["tag"],
    usages: ["tag <add/del/ls> <name> <tag contents>"],
    allow: true,
    ratelimit: {},
    prerun: function() {
        this.tags = load_json("tags");
        let that = this
        athena.on("message", (msg) => {
            if (!msg.guild) return;
            if (!that.tags[msg.guild.id]) return;
            let tags = that.tags[msg.guild.id].keys();
            let tag = false;
            tags.forEach((mytag) => {
                if((" "+msg.content.toLowerCase()+" ").indexOf(` ${mytag} `) > -1 && !tag) tag = mytag;
            });
            if (tag) {
                if (athena.user.id === msg.author.id) return;
                let tagData = that.tags[msg.guild.id][tag];
                msg.channel.send(tagData).catch(msg.channel.send);
                return;
            }
        });
    },
    run: (async function(data) {
        if (data.isDM) return;
        if (athena.auth.check(data.author.id, data.guild) < 2) return(data.reply("Access denied!"));
        let cm = data.args[0] ? data.args.splice(0, 1)[0] : false;
        let cm1 = data.args[0] ? data.args.splice(0, 1)[0] : false;
        let cm2 = data.args[0] ? data.args.splice(0, data.args.length).join(" ") : false;
        if (["add", "+", "set"].includes(cm)) {
            if (!cm1) return(data.reply("Missing arguement 1"));
            if (!cm2) return(data.reply("Missing arguement 2"));
            if (!this.tags[data.guild.id]) this.tags[data.guild.id] = {};
            this.tags[data.guild.id][cm1.toLowerCase()] = cm2;
            save_json("tags", this.tags);
            data.send("Tag successfully set!");
        }
        if (["del", "-", "unset"].includes(cm)) {
            if (!cm1) return;
            if (this.tags[data.guild.id].keys().includes(cm1.toLowerCase())) {
                delete this.tags[data.guild.id][cm1.toLowerCase()];
                data.send("Tag successfully removed!");
            }
            save_json("tags", this.tags);
        }
        if (["ls", "list", "~"].includes(cm)) {
            let page = cm1 || 1;
            // todo
        }
    }),
    onEnable: function() {
        //
    },
    onDisable: function() {
        //
    }
};
module.exports = {
    name: "iWarn",
    desc: "Warn a user and keep track of warnings",
    usage: "[warn/(clear)warns]",
    hidden: false,
    commands: ["warn", "clearwarns", "warns"],
    usages: ["warn <@user>", "clearwarns <@user>", "warns"],
    prerun: (function() {
        //
    }),
    run: (async function(data) {
        if (data.guild.authCheck(data.author.id) < 2) return;
        let warned = load_json("warnings");
        let change = false;
        if (!warned[data.guild.id]) warned[data.guild.id] = {};
        let cmd = data.cmd;
        if (["warn"].indexOf(cmd) > -1) {
            let users = data.mentions.users.array();
            if (users.length === 0) return (data.reply("Please mention a user to warn!"));
            let user = users[0];
            if(!warned[data.guild.id][user.id]) warned[data.guild.id][user.id] = 0;
            warned[data.guild.id][user.id]++;
            change = true;
            data.channel.send(`<@${user.id}> You have been warned, this is warnng #${warned[data.guild.id][user.id]}!`);
        }
        if (["clearwarns"].indexOf(cmd) > -1) {
            let users = data.mentions.users.array();
            if (users.length === 0) return (data.reply("Please mention a user to clear warnings of!"));
            let user = users[0];
            if(warned[data.guild.id][user.id]) delete warned[data.guild.id][user.id];
            change = true;
            data.channel.send(`<@${user.id}> your warnings have been cleared!`);
        }
        if (["warns"].indexOf(cmd) > -1) {
            if(!warned[data.guild.id]) return(data.channel.send("**Warnings**:```\n\n```"));
            let msg = "**Warnings**:```\n";
            Object.keys(warned[data.guild.id]).forEach((userID) => {
                let user = athena.users.get(userID);
                msg+=`${user.username} has ${warned[data.guild.id][userID]} warning(s)!\n`;
            });
            msg+="```";
            data.channel.send(msg);
        }
        if (change) save_json("warnings", warned);
    }),
    onEnable: (function() {
        //
    }),
    onDisable: (function() {
        //
    })
};
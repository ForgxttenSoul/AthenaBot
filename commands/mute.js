module.exports = {
    name: "Mute",
    desc: "Mute a user",
    usage: "[mute/unmute]",
    hidden: true,
    commands: ["`mute", "`unmute"],
    usages: [" "],
    prerun: (function() {
        let that = this;
        setInterval(() => {
            let muted = load_json("muted");
            let ct = muted.keys().length;
            muted.keys().forEach(person => {
                let unmute = muted[person];
                let [serverID, userID] = person.split(":");
                if (new Date().getTime() > unmute) {
                    let guild = athena.guilds.get(serverID);
                    if (guild === undefined) {
                        delete muted[person];
                        return;
                    }
                    guild.members.get(userID).removeRole(guild.roles.find("name", "<~Muted~>")).catch(console.log);
                    delete muted[person];
                }
            });
            if (ct !== muted.keys().length) {
                save_json("muted", muted);
            }
        }, 1000);
    }),
    run: (async function(data) {
        if (data.guild.authCheck(data.author.id) < 2) {
            return;
        }
        let [arg1, arg2, arg3] = data.args;
        let mentions = data.mentions.users.array();
        if (arg1 === "init") {
            let mute = data.guild.roles.find("name", "<~Muted~>");
            if (!mute) {
                data.guild.createRole({
                    name: "<~Muted~>"
                });
                mute = data.guild.roles.find("name", "<~Muted~>");
            }
            let channels = data.guild.channels.array();
            channels.forEach(async (channel) => {
                await channel.overwritePermissions(mute, {
                    "SEND_MESSAGES": false
                });
            });
            data.reply("Mute role setup!");
            return;
        }
        let isSetup = data.guild.roles.find("name", "<~Muted~>") !== undefined;
        if (isSetup) {
            if (data.mentions.users.size !== 1) {
                if (data.mentions.users.size === 0) {
                    data.reply("`::mute @user time:60` (seconds)");
                } else {
                    data.reply("Please only mute 1 user.");
                }
            } else {
                let cmds = cmdparse(data.content);
                cmds.splice(0, 1);
                let time = 1000 * 60 * 60 * 2;
                cmds.forEach(cc => {
                    let carg = cc.split(":") || [0];
                    if (carg[0] === "time") {
                        time = parseInt(carg[1]) * 1000;
                    }
                });
                let muteStart = new Date().getTime();
                let muteEnd = muteStart + time;
                let muted = load_json("muted");
                muted[data.guild.id + ":" + data.mentions.users.array()[0].id] = muteEnd;
                save_json("muted", muted);
                data.guild.members.get(data.mentions.users.array()[0].id).addRole(data.guild.roles.find("name", "<~Muted~>"));
                data.reply("Muted <@" + data.mentions.users.array()[0].id + "> for " + (time / 1000) + " seconds.");
            }
        } else {
            data.reply("Please run `::mute init` to setup mute system.\nUse that command when making new channels too.");
        }
        console.log({
            mentions
        });
    }),
    onEnable: (function() {
        //
    }),
    onDisable: (function() {
        //
    })
};
module.exports = {
    name: "Moderation",
    desc: "Server Moderator tools",
    usage: "[(un)ban(s)/kick/prune]",
    hidden: false,
    commands: ["ban", "kick", "unban", "prune", "bans"],
    usages: [" "],
    prerun: function() {
        //
    },
    run: (async (data) => {
        let [arg1, arg2, arg3] = data.args;
        if (data.cmd === "prune") {
            if ([2, 3, 4, 5].indexOf(data.guild.authCheck(data.author.id)) === -1) return;
            if (!arg1) {
                data.reply("Please provide the number of messages to prune!")
                    .then(msg => setTimeout(() => msg.delete(), 2500));
                return;
            }
            data.channel.bulkDelete(parseInt(arg1) + 1).then($ => {
                data.reply("Pruned " + (parseInt(arg1)) + " messages.")
                    .then(msg => setTimeout(() => msg.delete(), 3000));
            });
        }
        if (data.cmd === "ban") {
            if ([3, 4, 5].indexOf(data.guild.authCheck(data.author.id)) === -1) return;
            let mentions = data.mentions.users.array();
            if (mentions.length === 0) return (data.reply("Please mention users to ban!").then(msg => setTimeout(() => msg.delete(), 2500)));
            mentions.forEach(mention => {
                let member = data.guild.members.get(mention.id);
                member.ban(7);
            });
            data.reply("User(s) banned!").then((m) => setTimeout(() => m.delete(), 2500));
        }
        if (data.cmd === "unban") {
            if ([3, 4, 5].indexOf(data.guild.authCheck(data.author.id)) === -1) return;
            if (data.args.length === 0) return (data.reply("Please provide id's of users to unban!").then(msg => setTimeout(() => msg.delete(), 2500)));
            data.args.forEach(arg => {
                data.guild.unban(arg);
            });
            data.reply("User(s) unbanned!").then((m) => setTimeout(() => m.delete(), 2500));
        }
        if (data.cmd === "kick") {
            if ([2, 3, 4, 5].indexOf(data.guild.authCheck(data.author.id)) === -1) return;
            //
        }
        if (data.cmd === "bans"){
            if ([2, 3, 4, 5].indexOf(data.guild.authCheck(data.author.id)) === -1) return;
            let bans = (await data.guild.fetchBans()).array();
            let users = [];
            bans.forEach(user => {
                users.push(user.username);
            });
            data.channel.send(`**Banned Members**: \`\`\`\n${users.join("\n")}\n\`\`\` `);
        }
    }),
    onEnable: function() {
        //
    },
    onDisable: function() {
        //
    }
};
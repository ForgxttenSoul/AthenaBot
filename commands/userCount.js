module.exports = {
    name: "CountManager",
    desc: "Counts",
    usage: "[]",
    hidden: true,
    commands: ["ee"],
    usages: [" "],
    prerun: (function() {
        athena.allUsers = -1;
        let dio = require("discord.io");
        athena.dio = new dio.Client({
            token: config.token,
            autorun: true
        });
        athena.dio.on("ready", () => {
            console.log(`Discord.io Logged!`);
            athena.dio.getAllUsers();
            console.log();
        });
        athena.on("ready", () => {
            athena.dio.on("~guildMemberRemove", (member) => {
                console.log({member});
                let user = athena.fetchUser(member.id, false);
                console.log({user});
                if (!user) delete athena.dio.users[member.id];
            });
            athena.dio.on("any", () => {
                athena.userCount = athena.dio.users.keys().length;
            });
            athena.dio.on("error", console.log);
        });
    }),
    run: (async function(data) {
        data.channel.send([true].json());
    }),
    onEnable: (function() {
        //
    }),
    onDisable: (function() {
        //
    })
};
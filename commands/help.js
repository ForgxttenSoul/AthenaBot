// @ts-nocheck
module.exports = {
    name: "Help",
    desc: "Help system",
    usage: "[help/usage]",
    hidden: false,
    commands: ["help", "usage"],
    usages: ["help <page num>", "usage <topic>"],
    prerun: function() {
        //
    },
    run: async function(data) {
        if (data.cmd === "help") {
            console.log(1);
            let page = data.args[0] ? data.args[0] : 1;
            let curr_page = page - 1;
            if (curr_page < 0) {
                curr_page = 0;
            }
            let cs = athena.commands.coms.keys();
            let fields = [];
            cs.forEach((name, index) => {
                if (athena.commands.coms[name].hidden) {
                    return;
                }
                fields.push({
                    title: name,
                    value: athena.commands.coms[name].usage,
                    inline: true
                });
            });
            let max_page = (fields.length / 12).ceil();

            let help_embed = newEmbed({
                title: "-=-=-=-=-=-=- Help Menu -=-=-=-=-=-=-",
                desc: "Use " + config.prefix[0] + "usage [topic/command] `" + config.prefix[0] + "usage Announcer`",
                fields: fields.splice(12 * curr_page, 12),
                color: 0xf0f,
                footer: `Page ${curr_page+1}/${max_page}`
            });
            let help_msg = await data.channel.send({
                embed: help_embed
            });
            //await help_msg.react("⏪");
            await help_msg.react("⏹");
            //await help_msg.react("⏩");
            help_msg.awaitReactions(async (reaction, user) => {
                if (user.id !== data.author.id) {
                    return (false);
                }
                switch (reaction.emoji.name) {
                    case "⏪":
                        console.log("[Back]");
                        break;
                    case "⏹":
                        console.log("[Stop]");
                        help_msg.delete();
                        break;
                    case "⏩":
                        console.log("[Forward]");
                        //
                        console.log({
                            input: help_msg.embeds
                        });
                        break;
                    default:
                        break;
                }
            }, {
                time: (1000 * 60 * 5)
            });
            /*
            // Create a reaction collector
            const filter = (reaction, user) => reaction.emoji.name === '👌' && user.id === 'someID'
            message.awaitReactions(filter, { time: 15000 })
                .then(collected => console.log(`Collected ${collected.size} reactions`))
                .catch(console.error);
            */
        }
        if (data.cmd === "usage") {
            let name = data.args[0] || false;
            if (!name) {
                return;
            }
            name = name.toLowerCase();
            let names = athena.commands.coms.keys();
            let lowerNames = [];
            names.forEach((a, b) => lowerNames.push(a.toLowerCase()));
            let index = lowerNames.indexOf(name);
            let com = names[index];
            if (index === -1) {
                names = athena.commands.commands.keys();
                lowerNames = [];
                names.forEach((a, b) => lowerNames.push(a.toLowerCase()));
                index = lowerNames.indexOf(name);
                if (index === -1) {
                    data.send(":x:");
                    return;
                }
                name = names[index];
                com = athena.commands.commands[name];
            }
            com = athena.commands.coms[com];
            let {
                usage,
                desc,
                commands,
                hidden,
                usages
            } = com;
            let fields = [];
            usages.forEach(info => {
                fields.push({
                    title: "Command: " + info.split(" ")[0],
                    value: config.prefix[0] + info
                });
            });

            let help_msg = newEmbed({
                title: `Help, usage: ${com.name}`,
                desc,
                color: 0xff0,
                fields,
                footer: "Usage for " + com.name
            });
            help_msg = await data.channel.send({
                embed: help_msg
            });
            help_msg.react("⏹");
            help_msg.awaitReactions(async (reaction, user) => {
                if (user.id !== data.author.id) {
                    return (false);
                }
                switch (reaction.emoji.name) {
                    case "⏹":
                        help_msg.delete();
                        break;
                    case "⏩":
                    default:
                        break;
                }
            }, {
                time: (1000 * 60 * 5)
            });
        }
    },
    onEnable: function() {
        //
    },
    onDisable: function() {
        //
    }
};
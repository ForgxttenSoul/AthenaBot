const imageDownloader = require("image-downloader");
module.exports = {
    name: "CBackup",
    desc: "Backup and restore channels",
    usage: "[cb]",
    hidden: false,
    commands: ["cb"],
    usages: ["cb ..."],
    prerun: function() {
        //
    },
    run: (async (data) => {
        let [com1, com2, com3] = data.args;
        let channel = data.channel;
        if (![2, 3, 4, 5].includes(data.guild.authCheck(data.author.id))) return;
        let fetchAttachments = (async (channel) => {
            let content = [];
            let msgs = await channel.fetchMessages({
                limit: 100
            });
            while (msgs.size === 100) {
                msgs.array().forEach((msg) => {
                    msg.attachments.array().forEach((attachment) => {
                        content.push(attachment.url);
                    });
                });
                msgs = await channel.fetchMessages({
                    limit: 100,
                    before: msgs.last().id
                });
            }
            return (content);
        });
        const arg1 = data.args[0];
        if (arg1 === "backup") {
            let stat = await data.channel.send(`Fetching attachments...`);
            let urls = await fetchAttachments(data.channel);
            let m;
            await stat.edit(`Fetched attachments!`);
            let done = 0;
            await stat.edit(`Downloading attachments... 0/${urls.length} Complete!`);
            let t = new Date().getTime();
            let get = (i = 0) => {
                setTimeout(async () => {
                    let source = urls[i];
                    if (!source) return;
                    if (!fs.existsSync("./cb_data")) fs.mkdirSync("./cb_data");
                    let savePath = `${__dirname}/cb_data/${data.channel.id}`;
                    if (!fs.existsSync(savePath)) fs.mkdirSync(savePath);
                    console.log({
                        source
                    });
                    let target = `${savePath}/${sha256(`${source}:${new Date().getTime()}`)}.${source.split(".").pop()}`;
                    await imageDownloader({
                        url: source,
                        dest: target
                    });
                    done++;
                    if (new Date().getTime() - t > 2000) {
                        stat.edit(`Downloading attachments... ${done}/${urls.length} Complete!`);
                        t = new Date().getTime();
                    }
                    if (done === urls.length) {
                        
                        stat.edit(`Downloads complete!`);
                    }
                    get(i + 1);
                }, 1);
            };
            get();
        }
    }),
    onEnable: function() {
        //
    },
    onDisable: function() {
        //
    }
};
module.exports = {
    name: "Downloader",
    desc: "Download a file locally.",
    usage: "[dl]",
    hidden: true,
    commands: ["dl"],
    usages: ["dl <url>"],
    prerun: (bot) => {
        //
    },
    run: async (bot, data) => {
        if (!bot.config.owners.includes(data.author.id)) {
            return (data.reply("Unauthorized access!").then(msg => {
                setTimeout($ => msg.delete().catch(console.log), 2000);
            }).catch(console.log));
        }
        let op1 = data.args[0] || false;
        if (!op1) { return (data.reply("Insuffecient arguements.")); }
        if (op1 === "get") {
            let url = data.args[1] || false;
            let path = data.args[2] || false;
            if (!url || !path) { return (data.reply(`Usage: ${data.prefix}${data.cmd} get [url] [path]`)); }
            bot.download(url, path, (status) => {
                data.delete();
                if (status) {
                    data.reply("File successfully downloaded!");
                } else {
                    data.reply("Error downloading file!");
                }
            });
        }
    },
    onEnable: function () {
        //
    },
    onDisable: function () {
        //
    }
};
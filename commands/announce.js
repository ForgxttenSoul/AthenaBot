module.exports = {
    name: "Announcer",
    desc: "Send a PSA/Announcement to your server.",
    usage: "[psa]",
    hidden: false,
    commands: ["psa"],
    usages: ["psa <message>"],
    prerun: () => {
        //
    },
    run: async (data) => {
        data.delete();
        data.channel.send({
            embed: {
                color: 0x00ff00,
                fields: [
                    {
                        name: "Announcement",
                        value: data.args.join(" ")
                    }
                ]
            }
        }).catch(console.log);
    },
    onEnable: function () {
        //
    },
    onDisable: function () {
        //
    }
};

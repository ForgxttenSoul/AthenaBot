module.exports = {
    name: "Exec",
    desc: "Execute system commands.",
    usage: "[ex/exec]",
    hidden: true,
    commands: ["ex", "exec"],
    usages: ["ex <command>", "exec <command>"],
    prerun: function() {
        //
    },
    run: (async (data) => {
        if (![5].includes(data.guild.authCheck(data.author.id, data.guild))) {
            return (false);
        }
        const exec = require("child_process").exec;
        const $exec = (cmd, callback) => {
            exec(cmd, (error, stdout, stderr) => {
                if (error) {
                    callback(true, error);
                    return;
                }
                callback(false, stdout);
            });
        };
        (async () => {
            $exec(data.args.join(" "), (error, out) => {
                if (error) {
                    data.send(`❌ **Error**:\`\`\`\n${out}\n\`\`\``);
                    return;
                }
                data.send(`✅ **Success**:\`\`\`\n${out}\n\`\`\``);
            });
        })()
    }),
    onEnable: function() {
        //
    },
    onDisable: function() {
        //
    }
};
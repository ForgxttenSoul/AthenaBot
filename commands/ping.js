module.exports = {
    name: "Pinger",
    desc: "Ping the bot!",
    usage: "[ping/+]",
    hidden: false,
    commands: ["ping", "+"],
    usages: ["ping [!]"],
    prerun: () => {
        //
    },
    last: 0,
    run: async function (data) {
        let record = (one = "-", two = "-", three = "-") => `Pong! \`\`\`\nTime1: ${one} ms\nTime2: ${two} ms\nTime3: ${three} ms\n\`\`\``;
        let multiple = data.args[0] || false;
        if (multiple === "!") {
            let now = new Date().getTime();
            if ((now - this.last) < (1000 * 5)) {
                return (data.reply("Too fast, chill brah."));
            }
            this.last = now;
            let pings = [];
            let last = new Date().getTime();
            let msg = await data.channel.send(record());
            for (let i = 0; i < 3; i++) {
                let curr = new Date().getTime();
                pings.push((curr - last) / 2);
                last = curr;
                await msg.edit(record(...pings));
            }
        } else {
            let first = new Date().getTime();
            let msg = await data.channel.send("Pong! (- ms)");

            let second = new Date().getTime();
            first = (second - first) / 2;
            await msg.edit(`Pong! (${first} ms)`);
        }
    },
    onEnable: function () {
        //
    },
    onDisable: function () {
        //
    }
};
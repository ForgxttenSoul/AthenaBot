module.exports = {
    name: "Reboot",
    desc: "Restart the bot!",
    usage: "[reboot/restart]",
    hidden: true,
    commands: ["reboot", "restart"],
    usages: ["reboot ","restart "],
    prerun: function () {
        let that = this;
        this.greet = (() => {
            delete that.greet;
            if(config.reboot) {
                let message = config.reboot[0];
                let channel = config.reboot[1];
                delete config.reboot;
                save_json("config", config);
                athena.channels.get(channel)
                    .fetchMessage(message)
                    .then(msg => msg.edit("✅ Bot back online!"))
                    .catch(console.log);
            }
        });
        athena.on("ready", () => {
            console.log(1);
            if(that.greet) that.greet();
        });
    },
    run: (async (data) => {
        if([5].includes(data.guild.authCheck(data.author.id))){
            data.send("🔄 Rebooting...")
            .then((msg) => {
                (async ()=>{
                    config.reboot = [msg.id, data.channel.id];
                    save_json("config", config);
                })()
                .then(() => {
                    process.exit();
                });
            });
        }
    }),
    onEnable: function () {
        //
    },
    onDisable: function () {
        //
    }
};
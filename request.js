const $request = (url = "", data = {}, cb = $ => $) => {
    let https = url.startsWith("https://");
    url = url.split("").splice(https ? 8 : 7, url.split("").length).join("");
    const querystring = require("querystring");
    const re = require(https ? "https" : "http");
    const postData = querystring.stringify(data);
    url = url.split("/");
    const [$url] = url.splice(0, 1);
    url = `/${url.join("/")}`;
    const options = {
        hostname: $url,
        path: url,
        port: https ? 443 : 80,
        method: `${data.method || "get"}`,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    };
    if (data._auth) {
        options.headers["Authorization"] = `Bearer ${data._auth}`;
        delete data._auth;
    }
    var req = re.request(options, (res) => {
        let code = res.statusCode;
        if (code !== 200) {
            cb(`Error: ${code}`, true);
        }
        res.on("data", (d) => {
            if (code !== 200) return;
            cb(d, false);
        });
    });
    req.write(postData);
    req.end();
};
module.exports = $request;
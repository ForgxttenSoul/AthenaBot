// @ts-nocheck
let dperm = inp => {
    let permNames = {
        "create_instant_invite": 1,
        "kick_members": 2,
        "ban_members": 4,
        "administrator": 8,
        "manage_channels": 16,
        "manage_guild": 32,
        "add_reactions": 64,
        "view_audit_log": 128,
        "view_channel": 1024,
        "send_messages": 2048,
        "send_tts_message": 4096,
        "manage_messages": 8192,
        "embed_links": 16384,
        "attach_files": 32768,
        "read_message_history": 65536,
        "mention_everyone": 131072,
        "use_external_emojis": 262144,
        "connect": 1048576,
        "speak": 2097152,
        "mute_members": 4194304,
        "deafen_members": 8388608,
        "move_members": 16777216,
        "use_vad": 33554432,
        "change_nickname": 67108864,
        "manage_nicknames": 134217728,
        "manage_roles": 268435456,
        "manage_webhooks": 536870912,
        "manage_emojis": 1073741824
    };
    let permNums = {
        1: "create_instant_invite",
        2: "kick_members",
        4: "ban_members",
        8: "administrator",
        16: "manage_channels",
        32: "manage_guild",
        64: "add_reactions",
        128: "view_audit_log",
        1024: "view_channel",
        2048: "send_messages",
        4096: "send_tts_message",
        8192: "manage_messages",
        16384: "embed_links",
        32768: "attach_files",
        65536: "read_message_history",
        131072: "mention_everyone",
        262144: "use_external_emojis",
        1048576: "connect",
        2097152: "speak",
        4194304: "mute_members",
        8388608: "move_members",
        16777216: "change_nickname",
        33554432: "use_vad",
        67108864: "change_nickname",
        134217728: "manage_nicknames",
        268435456: "manage_roles",
        536870912: "manage_webhooks",
        1073741824: "manage_emojis"
    };
    let out = null;
    if (inp instanceof Array) {
        out = 0;
        inp.forEach((name, i) => {
            let n = permNames[name];
            console.log({ n, name, i: inp[i] });
            if (inp[i]) {
                out += n;
            }
        });
    } else if (inp instanceof Object) {
        out = 0;
        Object.keys(inp).forEach(name => {
            let n = permNames[name];
            if (inp[name]) {
                out += n;
            }
        });
    } else {
        out = {};
        Object.keys(permNums).reverse().forEach(num => {
            num = parseInt(num);
            let name = permNums[num];
            if (inp >= num) {
                out[name] = true;
                inp -= num;
            } else {
                out[name] = false;
            }
        });
    }
    return (out);
};
module.exports = dperm;
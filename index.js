const Discord = require("discord.js");
const fs = require("fs");
const root = __dirname;
const request = require("./request");
const cmdparse = require("./cmdparse");
const http = require("http");
const sha256 = require("sha256");
const dperms = require("./perm");
const msToTime = (inp) => {
    let timer = {
        year: 0,
        month: 0,
        week: 0,
        day: 0,
        hour: 0,
        minute: 0,
        second: 0,
        ms: 0
    };
    let second = 1000;
    let minute = second * 60;
    let hour = minute * 60;
    let day = hour * 24;
    let week = day * 7;
    let month = day * 30;
    let year = month * 12;
    while (inp >= year) {
        inp -= year;
        timer.year++;
    }
    while (inp >= month) {
        inp -= month;
        timer.month++;
    }
    while (inp >= week) {
        inp -= week;
        timer.week++;
    }
    while (inp >= day) {
        inp -= day;
        timer.day++;
    }
    while (inp >= hour) {
        inp -= hour;
        timer.hour++;
    }
    while (inp >= minute) {
        inp -= minute;
        timer.minute++;
    }
    while (inp >= second) {
        inp -= second;
        timer.second++;
    }
    timer.ms = inp;
    return (timer);
};

let sqldb = require("sqlite3objs");
let db = new sqldb("./configs/test.db");
if (!fs.existsSync("./configs/")) fs.mkdirSync("./configs/");
const json = ((obj) => typeof(obj) === "string" ? JSON.parse(obj) : JSON.stringify(obj, null, 4));
const load_json = ((path) => fs.existsSync(`${root}/configs/${path}.json`) ? json(fs.readFileSync(`${root}/${path}.json`).toString()) : {});
const save_json = ((path, obj) => fs.writeFileSync(`${root}/configs/${path}.json`, json(obj)));
let config = load_json("config");
if (json(config) === "{}") {
    config = {
        token: "insert_token_here",
        prefix: ["<>"],
        owners: [],
        status: []
    };
    save_json("config", config);
    console.log("Please setup config.json");
    process.exit();
}

String.prototype.json = (function(...input) {
    return (JSON.parse(this, ...input));
});

Number.prototype.round = (function() {
    return (Math.round(this));
});
Number.prototype.floor = (function() {
    return (Math.floor(this));
});
Number.prototype.ceil = (function() {
    return (Math.ceil(this));
});

Object.prototype.keys = (function() {
    return (Object.keys(this));
});
Object.prototype.values = (function() {
    return (Object.values(this));
});
Object.prototype.json = (function(...input) {
    return (JSON.stringify(this, ...input));
});

Array.prototype.json = (function(...input) {
    return (JSON.stringify(this, ...input));
});
Array.prototype.first = (function() {
    return (this[0]);
});
Array.prototype.last = (function() {
    return (this[this.length - 1]);
});

let garr = (($) => {
    let a = [];
    while (a.length < $) {
        a.push(0);
    }
    return (a);
});
let forall = ((num, cb) => garr(num).forEach(cb));

let online = false;
const isCmd = ((inp) => {
    let out = false;
    config.prefix.forEach(prefix => {
        if (inp.startsWith(prefix)) {
            out = prefix;
        }
    });
    return (out);
});
const $download = (async (url = "", dest = "", callback = ($ => $)) => {
    const file = fs.createWriteStream(dest);
    const req = url.startsWith("https") ? require("https") : require("http");
    req.get(url, (response) => {
        response.pipe(file);
        file.on("finish", () => {
            file.close(() => callback(true));
        });
        file.on("error", (err) => {
            fs.unlink(dest);
            callback(false, err.message);
        });
    });
});

const newEmbed = (({
    title,
    desc,
    fields,
    color,
    footer,
    timestamp,
    url,
    image_url,
    thumb_url,
    author
}) => {
    let embed = new Discord.RichEmbed();
    if (title !== undefined) embed.setTitle(title);
    if (desc !== undefined) embed.setDescription(desc);
    if (color !== undefined) embed.setColor(color);
    if (footer !== undefined) embed.setFooter(footer);
    if (timestamp !== undefined) embed.setTimestamp(timestamp);
    if (author !== undefined) {
        if (author instanceof String)  author = {
                name: author
            };
        embed.setAuthor(author);
    }
    if (image_url !== undefined) embed.setImage(image_url);
    if (url !== undefined) embed.setURL(url);
    if (thumb_url !== undefined) embed.setThumbnail(thumb_url);
    if (fields === undefined) fields = [];
    fields.forEach(field => {
        if ([true, false].includes(field)) {
            embed.addBlankField(field);
        } else {
            if (field.inline === undefined) {
                field.inline = false;
            }
            embed.addField(field.title, field.value, field.inline);
        }
    });
    return (embed);
});

let core = (async () => {
    online = true;
    const athena = new Discord.Client();
    athena.on("error", (e) => console.log(e));
    let start = 0;
    setTimeout(() => {
        athena.login(config.token);
        console.log("Logging in...");
    }, 2500);
    athena.on("ready", () => {
        start = new Date().getTime();
        console.log(`Logged in as ${athena.user.username}!`);
        let i = 0;
        athena.user.setActivity("Boot up sequence...", {
            type: "LISTENING"
        });
        athena.setInterval(() => {
            if (i === config.status.length) i = 0;
            let status = JSON.parse(JSON.stringify(config.status[i++]));
            let {
                name,
                options
            } = status;
            name = name.replace(/{server.count}/gim, athena.guilds.array().length);
            name = name.replace(/{user.count}/gim, athena.userCount);
            athena.user.setActivity(name, options);
        }, 1000 * 15);
    });
    athena.on("messageReactionAdd", (reactm, user) => {
        let {
            message,
            emoji
        } = reactm;
        if (!config.owners.includes(user.id)) return;
        if (reactm.message.author.id === athena.user.id) if (emoji.name === "❌") reactm.message.delete();
    });
    athena.auth = {
        listPerms: (function(userID, guild) {
            let p = [1];
            if (this.guilds[guild.id].mods.indexOf(userID) !== -1) p.push(2);
            if (this.guilds[guild.id].admins.indexOf(userID) !== -1) p.push(3);
            if (guild.owner.id === userID) p.push(4);
            if (config.owners.includes(userID)) p.push(5);
            return (p);
        }),
        check: (function(userID, guild) {
            let rank = 1;
            let gmod = this.guilds[guild.id].mods.indexOf(userID);
            if (gmod > -1) rank = 2;
            let gadmin = this.guilds[guild.id].admins.indexOf(userID);
            if (gadmin > -1) rank = 3;
            if (guild.owner.id === userID) rank = 4;
            if (config.owners.includes(userID)) rank = 5;
            return (rank);
        }),
        registerGuild: (function(guild) {
            this.guilds[guild.id] = {
                admins: [],
                mods: [],
                coms_off: []
            };
        }),
        addAdmin: (function(id, guild) {
            if (!this.guilds[guild.id]) this.registerGuild(guild);
            if (!this.guilds[guild.id].admins.includes(id)) {
                this.guilds[guild.id].admins.push(id);
                this.save();
                return (true);
            }
            return (false);
        }),
        addMod: (function(id, guild) {
            if (!this.guilds[guild.id]) {this.registerGuild(guild);
            if (!this.guilds[guild.id].mods.includes(id)) {
                this.guilds[guild.id].mods.push(id);
                this.save();
                return (true);
            }
            return (false);
        }),
        delAdmin: (function(id, guild) {
            if (!this.guilds[guild.id]) this.registerGuild(guild);
            let index = this.guilds[guild.id].admins.indexOf(id);
            if (index !== -1) {
                this.guilds[guild.id].admins.splice(index, 1);
                this.save();
                return (true);
            }
            return (false);
        }),
        delMod: (function(id, guild) {
            if (!this.guilds[guild.id])  this.registerGuild(guild);
            let index = this.guilds[guild.id].mods.indexOf(id);
            if (index !== -1) {
                this.guilds[guild.id].mods.splice(index, 1);
                this.save();
                return (true);
            }
            return (false);
        }),
        disableCom: (function(userID, guild, cmd) {
            if (!this.guilds[guild.id]) this.registerGuild(guild);
            let name = athena.commands.commands[cmd] || athena.commands.coms[cmd] || false;
            if (!name) return (false);
            if (![3, 4, 5].includes(this.check(userID, guild))) return (false);
            if (!this.guilds[guild.id].coms_off.includes(name)) {
                this.guilds[guild.id].coms_off.push(name);
                this.save();
            }
            return (true);
        }),
        enableCom: (function(userID, guild, cmd) {
            if (!this.guilds[guild.id]) this.registerGuild(guild);
            let name = athena.commands.commands[cmd] || athena.commands.coms[cmd] || false;
            if (!name) return (false);
            if (![3, 4, 5].includes(this.check(userID, guild))) return (false);
            let index = this.guilds[guild.id].coms_off.indexOf(name);
            if (index !== -1) {
                this.guilds[guild.id].coms_off.splice(index, 1);
                this.save();
            }
            return (true);
        }),
        save: (function() {
            save_json("guildauth", this.guilds);
        }),
        load: (function() {
            this.guilds = load_json("guildauth");
        }),
        guilds: {}
    };
    athena.auth.load();
    athena.commands = {
        commands: {}, // user commands
        coms: {}, // command main names
        enabled: [], // enabled commands
        save: (function() {
            save_json("commands", [this.enabled]);
        }),
        load: (function() {
            this.enabled = load_json("commands")[0] || [];
        }),
        enable: (function(cmd) {
            let name = this.commands[cmd] || this.coms[cmd];
            if (!this.enabled.includes(name)) {
                this.enabled.push(name);
                this.coms[name].onEnable();
            }
            this.save();
        }),
        disable: (function(cmd) {
            let name = this.commands[cmd] || this.coms[cmd];
            if (this.enabled.includes(name)) {
                this.enabled.splice(this.enabled.indexOf(name), 1);
                this.coms[name].onDisable();
            }
            this.save();
        }),
        register: (function(data) {
            let that = this;
            data.commands.forEach((command) => that.commands[command] = data.name);
            if (!this.enabled.includes(data.name)) this.enabled.push(data.name);
            this.coms[data.name] = data;
            data.prerun(athena);
            this.save();
        }),
        deregister: (function(cmd) {
            let name = this.commands[cmd] || this.coms[cmd];
            if (!cmd) return;
            cmd = this.coms[name];
            let that = this;
            cmd.commands.forEach((_name) => {
                let i = that.enabled.indexOf(_name);
                if (i > -1) that.enabled.splice(i, 1);
                delete that.commands[_name];
            });
            delete this.coms[name];
            Object.keys(athena.auth.guilds).forEach((gid) => {
                let guild = athena.auth.guilds[gid];
                let index = guild.coms_off.indexOf(name);
                if (index !== -1) athena.auth.guilds[gid].coms_off.splice(index, 1);
            });
            athena.auth.save();
            this.save();
        }),
        refresh: (function(name) {
            this.deregister(name);
            this.register();
        })
    };
    athena.commands.load();
    if (fs.existsSync("./commands")) {
        console.log(`\nLoading commands...\n`);
        let files = fs.readdirSync("./commands");
        files.forEach((file) => {
            (async () => {
                try {
                    const out = eval(fs.readFileSync(`./commands/${file}`).toString());
                    athena.commands.register(out);
                    console.log(`Loaded ${file}!`);
                } catch (e) console.log(e);
            })().catch(console.log);
        });
    }
    athena.on("message", (data) => {
        if (data.author.id === athena.user.id) return;
        let args = cmdparse(data.content);
        let prefix = isCmd(args[0]);
        (data === "=+=") ? data.reply(` \`\`\`\n${json({athena:true})}\n\`\`\` `): false;
        if (!prefix) return;
        let cmd = args[0].substr(prefix.length, args[0].length);
        args.splice(0, 1);
        data.prefix = prefix;
        data.cmd = cmd;
        data.args = args;
        data.guild.authCheck = ($) => athena.auth.check($, data.guild);
        data.send = ($) => data.channel.send($);
        if (!athena.commands.commands[cmd]) return;
        let name = athena.commands.commands[cmd];
        if (!athena.commands.enabled.includes(name))
            return (data.reply("Command globally disabled! Try again later!"));
        if (!athena.auth.guilds[data.guild.id])
            athena.auth.registerGuild(data.guild);
        if (athena.auth.guilds[data.guild.id].coms_off.includes(athena.commands.commands[cmd]))
            return (data.reply("Command locally disabled! Ask a server admin to enable!"));
        let com = athena.commands.commands[cmd];
        com = athena.commands.coms[com];
        data.isDM = (data.channel.guild === undefined);
        if (data.isDM) data.guild = data.author;
        com.run(data).catch((e) => {
            console.log(e);
            athena
                .users
                .get(config.owners[0])
                .send(`An error occured with comamnd: {${cmd}} : ${e}`);
        });
    });
    // extras
    athena.on("Amessage", (msg) => {
        if (msg.author.id === "239233310574903297") {
            let message = msg.content.split("");
            message.forEach((l, i) => (i % 2 === 1) message[i] = message[i].toUpperCase());
            msg.channel.send(message.join(""));
        }
    });
    athena.on("Amessage", (msg) => {
        return (0);
        if (msg.author.id === "239233310574903297" && msg.content.startsWith("> ")) {
            let print = msg.content.substr(2, msg.content.length);
            msg.delete();
        }
        if (msg.channel.id === "431818189735723039" && !msg.author.bot) {
            let ff = $ => msg.channel.send(print).then(sent => oldMessage = sent);
            if (!!oldMessage) oldMessage.delete().then(ff).catch(ff);
            else ff();
        }
    });
    athena.on("Amessage", (message) => {
        if (message.channel.id !== "461363351608623104") return;
        if (!message.guild.members.get(message.author.id).hasPermission(8)) {
            if (message.content.includes("http")) {
                message.channel.send("❌ Do not send links.").catch($ => $);
                message.delete().catch($ => $)
            }
        }
    });
    athena.on("Amessage", (data) => {
        if (data.guild.id !== "451901467931049984") {
            return;
        }
        let {
            attachments
        } = data;
        for (let attach of attachments) {
            let {
                url
            } = attach[1];
        }
    });
});

setTimeout(() => {
    if (!online) {
        core().catch(console.log)
    }
}, 1000);